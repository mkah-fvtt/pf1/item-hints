export class Hint {
	/**
	 * Plain label when icon nor image is not used. Used as tooltip hint with iconized enabled and no explicit hint provided.
	 */
	label;
	/**
	 * Font awesome icon.
	 */
	icon;
	/**
	 * Actual image file.
	 */
	image;
	/**
	 * Tooltip text.
	 */
	hint;
	/**
	 * Additional CSS classes.
	 */
	additionalClasses;
	/**
	 * If true, the hint is marked as 'tag' instead of 'hint', used by weapon properties.
	 */
	inBuiltTag;

	/**
	 * @param label Display label.
	 * @param {Array<string>} classes Array of HTML classes to add to identify the hint.
	 * @param {string} hint Tooltip text.
	 * @param {string} icon Font awesome icon class.
	 * @param {string} image URL to image file.
	 * @param {boolean} inbuilt Inbuilt tag. Used for weapon properties.
	 */
	constructor(label, classes = [], { hint = null, icon = null, image = null, inbuilt = false } = {}) {
		this.label = label;
		this.hint = hint;
		this.icon = icon;
		this.image = image;
		this.additionalClasses = classes;
		this.inBuiltTag = inbuilt;
	}

	clone() {
		return new this.constructor(this.label, [...this.additionalClasses], { hint: this.hint, icon: this.icon, image: this.image, inbuilt: this.inBuiltTag });
	}

	/**
	 * Same as new Hint(), just better for chaining.
	 *
	 * @param label Display label.
	 * @param {Array<string>} classes Array of HTML classes to add to identify the hint.
	 * @param {string} hint Tooltip text.
	 * @param {string} icon Font awesome icon class.
	 * @param {string} image URL to image file.
	 * @param {boolean} inbuilt Inbuilt tag. Used for weapon properties.
	 */
	static create(label, classes = [], { hint = null, icon = null, image = null, inbuilt = false } = {}) {
		return new Hint(...arguments);
	}

	/**
	 * @param {boolean} iconize Use icons or images where possible.
	 * @returns {HTMLElement} element for the hint
	 */
	element(iconize = false) {
		const IMAGE = 2, ICON = 1;

		const isIcon = iconize && (this.icon || this.image);
		const type = isIcon ? this.image ? IMAGE : ICON : 0;
		const typeMap = ['span', 'i', 'img'];

		if (this.icon && !Array.isArray(this.icon))
			this.icon = this.icon?.split(' ');

		let hint;

		const el = document.createElement(typeMap[type]);
		el.classList.add(this.inBuiltTag ? 'tag' : 'hint');
		switch (type) {
			case IMAGE: // image
				el.src = this.image;
				break;
			case ICON: // icon
				el.classList.add(...this.icon);
				break;
			default: // 0: span/text
				el.innerHTML = this.label;
				if (this.hint) hint = this.hint;
				break;
		}
		if (isIcon) {
			const tip = this.hint ?? this.label;
			if (tip) hint = tip;
			el.classList.add('icon');
		}
		else
			el.classList.add('tag');

		if (hint) {
			el.dataset.tooltip = hint.replace(/\n/g, '<br>');
			el.dataset.tooltipDirection = 'UP';
		}

		el.classList.add(...this.additionalClasses);

		return el;
	}
}
