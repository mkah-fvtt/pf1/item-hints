import { CFG } from './config.mjs';
import { Hint } from './hint.mjs';
import { exItemHandlers } from './common.mjs';

Hooks.once('init', () => {
	game.settings.register(CFG.id, CFG.SETTING.iconizeSetting, {
		name: 'Koboldworks.ItemHint.IconizeLabel',
		hint: 'Koboldworks.ItemHint.IconizeHint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
	});

	game.settings.register(CFG.id, CFG.SETTING.aura, {
		name: 'Koboldworks.ItemHint.AuraLabel',
		hint: 'Koboldworks.ItemHint.AuraHint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
	});

	game.settings.register(CFG.id, CFG.SETTING.schoolAura, {
		name: 'Koboldworks.ItemHint.IconizeSchoolLabel',
		hint: 'Koboldworks.ItemHint.IconizeSchoolHint',
		type: String,
		choices: {
			letters: 'Koboldworks.ItemHint.Icons.Letters',
			letterswcl: 'Koboldworks.ItemHint.Icons.LettersWithCL',
			custom: 'Koboldworks.ItemHint.Icons.Custom',
			runes: 'Koboldworks.ItemHint.Icons.Runes',
		},
		default: 'letters',
		config: true,
		scope: 'client',
	});

	game.settings.register(CFG.id, CFG.SETTING.showChanges, {
		name: 'Koboldworks.ItemHint.ChangeLabel',
		hint: 'Koboldworks.ItemHint.ChangeHint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
	});

	game.settings.register(CFG.id, CFG.SETTING.showAllValues, {
		name: 'Koboldworks.ItemHint.ValueLabel',
		hint: 'Koboldworks.ItemHint.ValueHint',
		type: String,
		choices: {
			tradegoods: 'Koboldworks.ItemHint.Value.TradeGoodsOnly',
			containers: 'Koboldworks.ItemHint.Value.TradeGoodsAndContainers',
			expanded: 'Koboldworks.ItemHint.Value.Expanded',
			all: 'Koboldworks.ItemHint.Value.All',
			none: 'Koboldworks.ItemHint.Value.None',
		},
		default: 'expanded',
		config: true,
		scope: 'client',
	});

	game.settings.register(CFG.id, CFG.SETTING.maxLabelLength, {
		name: 'Koboldworks.ItemHint.MaxLabelLength',
		hint: 'Koboldworks.ItemHint.MaxLabelLengthHint',
		type: Number,
		range: {
			min: 8,
			max: 80,
			step: 1,
		},
		default: 42,
		config: true,
		scope: 'client',
	});

	game.settings.register(CFG.id, CFG.SETTING.sanity, {
		name: 'Koboldworks.ItemHint.SanityLabel',
		hint: 'Koboldworks.ItemHint.SanityHint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
	});

	game.settings.register(CFG.id, CFG.SETTING.homebrew, {
		name: 'Koboldworks.ItemHint.HomebrewLabel',
		hint: 'Koboldworks.ItemHint.HomebrewHint',
		type: Boolean,
		default: false,
		config: true,
		scope: 'world',
	});

	game.settings.register(CFG.id, CFG.SETTING.unlang, {
		name: 'Koboldworks.ItemHint.LangLabel',
		hint: 'Koboldworks.ItemHint.LangHint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
	});

	game.settings.register(CFG.id, CFG.SETTING.compatibilityMode, {
		name: 'Koboldworks.ItemHint.CompatLabel',
		hint: 'Koboldworks.ItemHint.CompatHint',
		type: Boolean,
		default: false,
		config: true,
		scope: 'client',
	});

	const module = game.modules.get(CFG.id);

	module.api = {
		HintClass: Hint,
		addHandler: (callback) => exItemHandlers.push(callback),
	};

	console.log(`Item Hints 🎟 | ${module.version} | READY`);
});
