import './loader.mjs';
import './settings.mjs';
import { CFG } from './config.mjs';
import { createNode, itemHandlers, exItemHandlers } from './common.mjs';
import { Hint } from './hint.mjs';
import { Manager as handler } from './handlers/manager.mjs'; // preload handlers
import { renderCustomHintEditor } from './editor.mjs';
import { HandlerOptions } from './handler-options.mjs';

function getCompatibilityMode() {
	return game.settings.get(CFG.id, CFG.SETTING.compatibilityMode);
}

const elementHandler = async (el, actor, items, isContainer) => {
	const id = el.dataset.itemId;
	const item = items.get(id);
	if (!item) return;

	const nEl = el.querySelector('.item-name');
	if (!nEl) return;

	const hints = createNode('div', null, [CFG.CSS.branding, CFG.CSS.common]);

	const opts = new HandlerOptions(item, actor ?? item.actor, el);

	try {
		switch (item.type) {
			case 'feat':
				handler.damage(opts, hints);
				await handler.saves(opts, hints);
				handler.changes(opts, hints);
				handler.changeFlags(opts, hints);
				break;
			case 'spell':
				handler.damage(opts, hints);
				await handler.saves(opts, hints);
				await handler.spell(opts, hints);
				break;
			case 'consumable':
			case 'equipment':
			case 'loot':
			case 'weapon':
				handler.proficiency(opts, hints);
				handler.damage(opts, hints);
				handler.attack(opts, hints);
				handler.item(opts, hints);
				handler.changes(opts, hints);
				handler.changeFlags(opts, hints);
				await handler.saves(opts, hints);
				break;
			case 'attack':
				handler.proficiency(opts, hints);
				handler.damage(opts, hints);
				handler.attack(opts, hints);
				handler.item(opts, hints);
				await handler.saves(opts, hints);
				break;
			case 'buff':
				handler.changes(opts, hints);
				handler.changeFlags(opts, hints);
				break;
			case 'class':
				handler.class(opts, hints);
				handler.changes(opts, hints);
				handler.changeFlags(opts, hints);
				break;
			// case 'race':
			case 'container':
				handler.container(opts, hints);
				break;
			default:
				// NOP
				break;
		}
		handler.action(opts, hints);
		handler.value(opts, hints, isContainer);
		handler.scripts(opts, hints);
		await handler.customHints(opts, hints);

		itemHandlers.forEach(itemHandler => itemHandler(opts, hints));

		let handlerErrors = 0;
		exItemHandlers.forEach(exItemHandler => {
			let error = false;
			try {
				const exHints = exItemHandler(actor, item, opts) ?? [];
				for (const xh of exHints) {
					if (xh instanceof Hint) hints.append(xh.element(opts.iconize));
					else {
						console.error('ITEM HINTS | Bad return value', xh, 'from external handler:', exItemHandler, 'for', item.name, item);
						error = true;
						break;
					}
				}
			}
			catch (err) {
				console.error('ITEM HINTS | Error in external handler:\n', { actor, item, uuid: item.uuid, handler: exItemHandler }, '\n', err, '\nHANDLER REMOVED!');
				error = true;
			}
			finally {
				if (error) {
					handlerErrors++;
					exItemHandler.__ih_needsDelete = true;
				}
			}
		});
		if (handlerErrors) {
			for (let i = 0; i < exItemHandlers.length; i++) {
				if (exItemHandlers[i].__ih_needsDelete) {
					exItemHandlers.splice(i, 1);
					i--;
				}
			}
		}
	}
	catch (err) {
		console.warn('ITEM HINTS | FAILURE | ITEM:', item?.name, 'ID:', id, 'ITEM:', item, 'ELEMENT:', el);
		console.error(err);
	}

	// TODO: Strip unwanted hints based on... something?
	nEl.querySelector('h4').after(hints); // unreliable?
};

/**
 *
 * @param {Element} html
 * @param {Actor} actor
 * @param {Item[]} items
 * @param {boolean} isContainer
 */
async function injectHints(html, actor, items, isContainer) {
	const iEls = html.querySelectorAll('.item-list .item');
	iEls.forEach(el => elementHandler(el, actor, items, isContainer));
}

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 */
function renderActorSheetHints(sheet, [html]) {
	const actor = sheet.actor;
	injectHints(html, actor, actor.items, false);
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 */
function renderItemSheetHints(sheet, [html]) {
	const item = sheet.item;
	if (item.type !== 'container') return;
	injectHints(html, item.actor, item.items, true);
}

Hooks.on('renderItemSheet', renderCustomHintEditor);

Hooks.once('ready', () => {
	const hid0 = Hooks.on('renderActorSheetPF', renderActorSheetHints);
	const hid1 = Hooks.on('renderItemSheet', renderItemSheetHints);

	// Re-order hook handlers to be the first
	const events0 = Hooks.events.renderActorSheetPF;
	const idx0 = events0.findIndex(h => h.id === hid0);
	const [handler0] = events0.splice(idx0, 1);
	events0.unshift(handler0);

	const events1 = Hooks.events.renderItemSheet;
	const idx1 = events1.findIndex(h => h.id === hid1);
	const [handler1] = events1.splice(idx1, 1);
	events1.unshift(handler1);
});
