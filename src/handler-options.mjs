import { CFG } from './config.mjs';

export class HandlerOptions {
	/** @type {Element} */
	element;
	/** @type {Actor} */
	actor;
	/** @type {Item} */
	item;

	/** @type {object} actor.system */
	get actorData() { return this.actor?.system; }
	/** @type {object} item.system */
	get itemData() { return this.item?.system; }

	/* @type {ItemAction} */
	get defaultAction() { return this.item?.defaultAction; }

	get isWeapon() { return this.item.type === 'weapon'; }
	/** @type {boolean} */
	get isIdentified() { return this.itemData.identified ?? true; }
	/** @type {number} */
	get enhancement() { return this.itemData.enh ?? this.itemData.armor?.enh; }
	/** @type {number} */
	get casterLevel() { return this.itemData.cl; }
	/** @type {boolean} */
	get showSecrets() { return this.isIdentified || game.user.isGM; }

	#rollData;
	/** @type {object} Roll data */
	get rollData() {
		this.#rollData ??= this.item.getRollData({ forceRefresh: false });
		return this.#rollData;
	}

	iconize = false;
	sanity = false;
	homebrew = false;
	changes = false;
	values = false;

	/**
	 * @param {Item} item
	 * @param {Actor} actor
	 * @param {Element} element
	 */
	constructor(item, actor, element) {
		this.item = item;
		this.actor = actor;
		this.element = element;

		this.iconize = game.settings.get(CFG.id, CFG.SETTING.iconizeSetting);
		this.sanity = game.settings.get(CFG.id, CFG.SETTING.sanity);
		this.homebrew = game.settings.get(CFG.id, CFG.SETTING.homebrew);
		this.changes = game.settings.get(CFG.id, CFG.SETTING.showChanges);
		this.values = game.settings.get(CFG.id, CFG.SETTING.showAllValues);
	}
}
