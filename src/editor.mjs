import { CFG } from './config.mjs';
import { renderIHTemplate, getHint } from './common.mjs';
import { handleCustomHints } from './handlers/custom.mjs';

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 */
export function renderCustomHintEditor(sheet, [html]) {
	const item = sheet.item, actor = sheet.actor;
	if (sheet.__modItemHintsDisableCustom) return;

	async function _onCustomHintChange(ev) {
		const newValue = ev.target.value.trim();
		return newValue?.length > 0 ? item.setFlag('world', CFG.SETTING.customHint, newValue) : item.unsetFlag('world', CFG.SETTING.customHint);
	}

	const advTab = html.querySelector('.tab[data-tab="advanced"]');
	if (!advTab) return;

	try {
		const value = getHint(item);

		const templateData = {
			value,
		};

		const [node] = renderIHTemplate('item-hints-editor', templateData);

		if (templateData.value?.length) {
			const example = node.querySelector('.example .item-hints');

			const options = {
				item,
				actor,
				get rollData() { return this.item.getRollData(); },
			};
			handleCustomHints(options, example);
		}

		const input = node.querySelector('input');
		if (sheet.isEditable)
			input.addEventListener('change', _onCustomHintChange);
		else {
			input.readOnly = true;
			input.disabled = true;
		}

		advTab.append(node);
	}
	catch (err) {
		console.warn(`ITEM HINTS | Editor | ${item.name}:`, item);
		console.debug('ITEM HINTS | Editor | Error detected; Temporarily disabling for item:', item.name);
		console.error(err);
		sheet.item.__modItemHintsDisableCustom = true; // Not meant to be stored in database
	}
}
