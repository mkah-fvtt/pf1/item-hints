import { CFG } from './config.mjs';
import { Hint } from './hint.mjs';

export const itemHandlers = [];
export const exItemHandlers = [];

/**
 * @param {string} id - Template ID
 * @param {object} data - Template data
 * @returns {NodeList<Element>}
 */
export function renderIHTemplate(id, data) {
	const handlebarsOptions = {
		allowProtoMethodsByDefault: true,
		allowProtoPropertiesByDefault: true,
		preventIndent: true,
	};

	const d = document.createElement('div');
	d.innerHTML = Handlebars.partials[id](data, handlebarsOptions);
	return d.childNodes;
}

export async function migrateHints(item) {
	// Test if we can edit
	if (!item.isOwner) return;
	const pack = game.packs.get(item.pack);
	if (pack?.locked) return;

	// Migrate proper
	const key = CFG.SETTING.customHint;
	const hint = item.getItemDictionaryFlag(key);
	if (hint) {
		console.log('Item Hints 🎟 | Migration |', item.name, item.id);
		const itemUpdate = {
			system: {
				flags: {
					dictionary: {
						[`-=${key}`]: null,
					},
				},
			},
			flags: {
				world: {
					[key]: hint,
				},
			},
		};
		return item.update(itemUpdate, { render: false });
	}
}

export function getHint(item) {
	let customHints = item.getItemDictionaryFlag(CFG.SETTING.customHint);
	if (customHints) migrateHints(item);
	customHints ||= item.getFlag('world', CFG.SETTING.customHint);
	return customHints;
}

export function createNode(node = 'span', text = '', classes = [], { tooltip = null } = {}) {
	const n = document.createElement(node);
	n.textContent = text;
	if (classes.length) n.classList.add(...classes);
	if (tooltip) n.dataset.tooltip = tooltip;
	return n;
}

export const descriptorIcons = {
	material: new Hint('', ['material'], { icon: 'fas fa-coins' }),
	value: new Hint('', ['value'], { icon: 'fas fa-coins' }),
	inconsistency: new Hint('', ['inconsistency'], { icon: 'fas fa-exclamation-circle' }),
};
