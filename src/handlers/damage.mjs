import { Hint } from '../hint.mjs';
import { i18n } from '../i18n.mjs';

/**
 * @param {HandlerOptions} o Options
 * @param hints
 */
export function handleDamage(o, hints) {
	const item = o.item;
	if (!item.hasDamage) return;

	if (o.itemData.nonlethal) {
		hints.append(
			Hint.create(
				i18n.get('Koboldworks.Generic.Nonlethal'),
				['nonlethal'],
				{ icon: 'far fa-hand-peace' },
			).element(o.iconize),
		);
	}
}
