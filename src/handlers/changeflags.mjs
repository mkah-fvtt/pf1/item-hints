// Change Flags

import { Hint } from '../hint.mjs';
import { i18n } from '../i18n.mjs';

/**
 * @param {HandlerOptions} opt Options
 * @param hints
 */
export function handleChangeFlags(opt, hints) {
	if (!opt.changes) return;
	if (!opt.isIdentified) return;
	const flags = opt.itemData.changeFlags;
	if (!flags) return;
	if (flags.heavyArmorFullSpeed && flags.mediumArmorFullSpeed) hints.append(Hint.create('+ArmorSpd', ['change']).element());
	else if (flags.heavyArmorFullSpeed) hints.append(Hint.create('+ArmorSpd (Hvy)', ['change']).element());
	else if (flags.mediumArmorFullSpeed) hints.append(Hint.create('+ArmorSpd (Med)', ['change']).element());
	if (flags.loseDexToAC) hints.append(Hint.create(`-${i18n.get('Koboldworks.Short.DexterityToAC')}`, ['change', 'penalty']).element());
	if (flags.noEncumbrance) hints.append(Hint.create(`-${i18n.get('PF1.Encumbrance')}`, ['change']).element());
}
