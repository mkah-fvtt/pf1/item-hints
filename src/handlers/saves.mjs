// Feats and spells

import { Hint } from '../hint.mjs';
import { i18n } from '../i18n.mjs';

/**
 * @param {HandlerOptions} o Options
 * @param hints
 */
export async function handleSaves(o, hints) {
	const item = o.item;
	if (!item.hasSave) return;

	const itemData = o.itemData,
		itemFormula = itemData.save.dc,
		bookFormula = item.spellbook?.baseDCFormula ?? '',
		dcFormula = [bookFormula, itemFormula].map(f => f?.trim()).filter(f => !!f).join(' + ');

	const rollData = o.rollData;

	if (dcFormula) {
		/* global RollPF */
		const idc = RollPF.safeRoll(itemFormula, rollData);
		if (item.type !== 'spell' || idc.total !== 0) {
			const rd = RollPF.safeRoll(dcFormula, rollData);
			// const dc = item.getDC(o.rollData);
			const dc = !rd.err ? rd.total : 'Error';
			const dcHint = Hint.create(`${i18n.get('PF1.DC')} ${dc}`, ['dc'], { hint: rd.formula });
			hints.append(dcHint.element(o.iconize));
		}
	}

	// Save description
	const save = itemData.save?.description;
	if (save?.length && !/harmless/i.test(save))
		hints.append(Hint.create(await pf1.utils.enrichHTMLUnrolled(save, { rollData }), ['save']).element());
}
