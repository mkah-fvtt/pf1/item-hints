import { Hint } from '../hint.mjs';
import { i18n } from '../i18n.mjs';

/**
 * @param {HandlerOptions} o Options
 * @param hints
 */
export function handleAction(o, hints) {
	const item = o.item;
	if (!['attack', 'weapon', 'spell'].includes(item.type)) return;

	const itemData = o.itemData;

	if (itemData.actions !== undefined && !item.hasAction)
		hints.append(Hint.create(i18n.get('Koboldworks.Generic.NoAction'), ['warning', 'no-action']).element());

	if (itemData.actions?.some(a => a.usesAmmo && !a.ammoType))
		hints.append(Hint.create(i18n.get('Koboldworks.Generic.NoAmmoType'), ['warning', 'no-ammo-type']).element());
}
