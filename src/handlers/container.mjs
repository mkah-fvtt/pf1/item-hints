import { Hint } from '../hint.mjs';
import { i18n } from '../i18n.mjs';

export function handleContainer(o, hints) {
	const item = o.item,
		count = item.items.contents.length,
		subItems = item.items.find(i => i.type === 'container'),
		coins = o.itemData.currency ?? {},
		haveCoins = coins.pp > 0 || coins.gp > 0 || coins.sp > 0 || coins.cp > 0,
		more = subItems || haveCoins;

	const label = more ? i18n.get('Koboldworks.Hints.ContentsIndefinite', { quantity: count }) : i18n.get('Koboldworks.Hints.Contents', { quantity: count });

	const h = [label];
	if (subItems) h.push(i18n.get('PF1.InventoryContainers'));
	if (haveCoins) h.push(i18n.get('PF1.Currency.Label'));

	hints.append(Hint.create(label, ['item-count'], { hint: h.join(' + ') }).element());
}
