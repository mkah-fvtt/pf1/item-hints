import { handleAction } from './actions.mjs';
import { handleAttack } from './attack.mjs';
import { handleDamage } from './damage.mjs';
import { handleChangeFlags } from './changeflags.mjs';
import { handleChanges } from './changes.mjs';
import { handleClass } from './class.mjs';
import { handleContainer } from './container.mjs';
import { handleCustomHints } from './custom.mjs';
import { handleItem } from './item.mjs';
import { handleProficiency } from './proficiency.mjs';
import { handleSaves } from './saves.mjs';
import { handleSpell } from './spell.mjs';
import { handleValue } from './value.mjs';
import { handleScripts } from './scripts.mjs';

export const Manager = {
	action: handleAction,
	attack: handleAttack,
	damage: handleDamage,
	changeFlags: handleChangeFlags,
	changes: handleChanges,
	class: handleClass,
	container: handleContainer,
	customHints: handleCustomHints,
	item: handleItem,
	proficiency: handleProficiency,
	saves: handleSaves,
	spell: handleSpell,
	value: handleValue,
	scripts: handleScripts,
};
