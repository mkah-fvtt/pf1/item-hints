// Scripts

import { Hint } from '../hint.mjs';
import { i18n } from '../i18n.mjs';

/**
 * @param {HandlerOptions} o Options
 * @param hints
 */
export function handleScripts(o, hints) {
	const itemData = o.itemData;

	if (itemData.scriptCalls?.length) {
		// <i class="fas fa-file-prescription"></i>
		// fab fa-node-js
		const scHint = Hint.create(i18n.get('Koboldworks.ItemHint.Description.ScriptCall'), ['script-call'], { icon: 'fas fa-file-prescription', hint: i18n.get('Koboldworks.ItemHint.Description.ScriptCallHint') });
		hints.append(scHint.element(o.iconize));
	}
}
