export const CFG = /** @type {const} */ ({
	id: 'mkah-pf1-item-hints',
	CSS: {
		branding: 'koboldworks',
		common: 'item-hints',
		props: 'hint-item-properties',
	},
	SETTING: {
		customHint: 'itemHints',
		iconizeSetting: 'iconize',
		compatibilityMode: 'compat',
		showAllValues: 'allValued',
		maxLabelLength: 'maxLabelLength',
		aura: 'aura',
		schoolAura: 'iconizeAura',
		showChanges: 'changes',
		sanity: 'sanity',
		homebrew: 'homebrew',
		unlang: 'nonstandardLang',
	},
});

const templates = {
	'item-hints-editor': `modules/${CFG.id}/templates/editor.hbs`,
};

Hooks.once('setup', () => loadTemplates(templates));
