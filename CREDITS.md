# Credits

## Icons

|Icon|Author|License|
|:---|:---|:---|
|Magic shield|_Lorc_|CC BY 3.0|
|Energy arrow|_Lorc_|CC BY 3.0|
|Frog prince|_Delapouite_|CC BY 3.0|
|Magic trick|_Delapouite_|CC BY 3.0|
|Sparkles|_Delapouite_|CC BY 3.0|
|Portal|_Lorc_|CC BY 3.0|
|Crystal ball|_Lorc_|CC BY 3.0|
|Brain tentacle|_Delapouite_|CC BY 3.0|
|Death juice|_DarkZaitzev_|CC BY 3.0|

## Thassilonian runes

Created by [Jeff Carlisle](https://twitter.com/jeffcarlisleart/status/435464540717715456) and owned by Paizo  
Distributed under Paizo's [Community Use Policy](https://paizo.com/community/communityuse) license.

Conversion to SVG by myself (Mana#4176) which does not alter the license.
