import fs from 'node:fs';
import path from 'node:path';
import { stat, readFile } from 'node:fs/promises';
import zlib from 'node:zlib';
import stream from 'node:stream';

const packData = fs.readFileSync('./package.json');
const packJSON = JSON.parse(packData);
const mainJsFile = packJSON.main;
const bundleJsFile = mainJsFile.replace(/\.mjs$/, '.bundled.mjs');
const bundleCssFile = mainJsFile.replace(/\.mjs$/, '.css');

const ignore = [
	/^node_modules$/,
	/\.bundled\.mjs$/,
	/\.lock$/,
	/\.js$/,
	/\.yml$/,
	/\.git$/,
];

const files = {
	all: [],
	files: [],
	folders: [],
	typed: {
		js: [],
		css: [],
		scss: [],
		hbs: [],
		lang: [],
		img: [],
		map: [],
		md: [],
		json: [],
	},
	ignored: [],
	unknown: [],
};

function scanFolder(currentPath) {
	const dirScan = fs.readdirSync(currentPath, { withFileTypes: true, encoding: 'utf8' });
	const sFolders = [], sFiles = [];
	for (const file of dirScan) {
		const fullPath = path.resolve(currentPath, file.name);
		const d = { name: file.name.toString(), path: fullPath, dir: currentPath };

		if (ignore.some(ign => ign.exec(d.name) != null)) {
			files.ignored.push(d);
			continue;
		}

		if (file.isDirectory()) sFolders.push(d);
		else if (file.isFile()) {
			d.ext = path.extname(d.name)?.slice(1) || null;
			sFiles.push(d);
		}
	}

	for (const f of sFiles) {
		files.all.push(f);
		switch (f.ext) {
			case 'mjs': files.typed.js.push(f); break;
			case 'css': files.typed.css.push(f); break;
			case 'scss': files.typed.scss.push(f); break;
			case 'hbs': files.typed.hbs.push(f); break;
			case 'md': files.typed.md.push(f); break;
			case 'map': files.typed.map.push(f); break;
			case 'png':
			case 'svg':
				files.typed.img.push(f);
				break;
			case 'json': {
				if (/[\\/]lang[\\/][^.]+\.json$/.test(f.path))
					files.typed.lang.push(f);
				else
					files.typed.json.push(f);
				break;
			}
			default: files.unknown.push(f); break;
		}
	}

	for (const d of sFolders) {
		files.folders.push(d);
		scanFolder(d.path);
	}
}

scanFolder('./');

// console.log(files.unknown);

const stats = {}, statPromises = [];
Object.keys(files.typed).forEach((cat) => stats[cat] ??= 0);

Object.entries(files.typed).forEach(([cat, files]) => {
	files.forEach(f => {
		const p = stat(f.path);
		statPromises.push(p);
		p.then(s => stats[cat] += s.size);
	});
});

await Promise.allSettled(statPromises);

async function getGzipSize(path) {
	return readFile(path).then(inp => zlib.gzipSync(inp)?.length);
}

console.log(stats);
const gzipJsSize = await getGzipSize(path.resolve('./', bundleJsFile));
const gzipCssSize = await getGzipSize(path.resolve('./css/', bundleCssFile));
const bundleJsI = await stat(bundleJsFile);
const bundleCssI = await stat(`./css/${bundleCssFile}`);
console.log('Bundled JS:', Math.floor(bundleJsI.size / 100) / 10, 'kB;', 'Sources:', Math.floor(stats.js / 100) / 10, 'kB in', files.typed.js.length, 'files');
console.log('Gzip:', Math.floor(gzipJsSize / 100) / 10, 'kB (', Math.round(gzipJsSize / bundleJsI.size * 100), '% )');
console.log('Bundled CSS:', Math.floor(bundleCssI.size / 100) / 10, 'kB;', 'Sources:', Math.floor(stats.scss / 100) / 10, 'kB in', files.typed.scss.length, 'files');
console.log('Gzip:', Math.floor(gzipCssSize / 100) / 10, 'kB (', Math.round(gzipCssSize / bundleCssI.size * 100), '% )');
